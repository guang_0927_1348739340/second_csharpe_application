﻿using System;

namespace Dome1
{
    class Program
    {
        static void Main(string[] args)
        {
            //位移
            int n = 2 << 2;
            Console.WriteLine("2的立方为：" + n);
            Console.WriteLine("");

            //定义圆周率
            const double a = Math.PI;
            //圆的边长
            int b = 6;
            //圆的面积
            double c = Math.Pow(b, 2);
            Console.WriteLine("圆的面积为：" + c);
            Console.WriteLine("");

            ///判断年份

            string tg = "甲乙丙丁戊己庚辛壬癸";     //天干
            string dz = "子丑寅卯辰巳午未申酉戌亥"; //地支
            string sx = "鼠牛虎兔龙蛇马羊猴鸡狗猪";  //生肖
            try   //捕捉异常
            {
                Console.WriteLine("请输入一个年份");
                int year = int.Parse(Console.ReadLine());
                if (year.ToString() != "" && year > 0)
                {
                    Console.WriteLine("农历：" + year + "年" + tg[(year - 4) % 10] + dz[(year - 4) % 12] + "年， 是" + sx[(year - 4) % 12] + "年");
                }
                else if (year == 0)
                {
                    Console.WriteLine("耶稣诞生");

                }
                else
                {
                    Console.WriteLine("请正确输入年份");
                }

            }
            catch (Exception v)  //反馈信息
            {

                Console.WriteLine(v.Message);  //提示索引超出范围
                Console.WriteLine("请数入数字，哥哥");       //自定义提示

            }
            Console.WriteLine("");

            //判断星期

            int week = 5;
            switch (week)
            {
                case 1:
                    Console.WriteLine("星期一");
                    break;
                case 2:
                    Console.WriteLine("星期二");
                    break;
                case 3:
                    Console.WriteLine("星期三");
                    break;
                case 4:
                    Console.WriteLine("星期四");
                    break;
                case 5:
                    Console.WriteLine("星期五");
                    break;
                case 6:
                    Console.WriteLine("星期六");
                    break;
                case 7:
                    Console.WriteLine("星期天");
                    break;
                default:
                    Console.WriteLine("世界末日");
                    break;

            }
            Console.WriteLine("");


            //打印杨辉三角

            Console.WriteLine("请输入行数");
            int rows = int.Parse(Console.ReadLine());
            //初始化二维数组
            int[][] nums = new int[rows][];
            for (int i = 0; i < rows; i++)
            {
                nums[i] = new int[i + 1];
            }
            //边界赋值
            for (int i = 0; i < rows; i++)
            {
                nums[i][0] = 1;
                nums[i][i] = 1;
            }
            //中心值赋值
            for (int i = 2; i < rows; i++) //控制行数
            {
                for (int j = 1; j < i; j++) //控制列数
                {
                    nums[i][j] = nums[i - 1][j] + nums[i - 1][j - 1]; //每一列的值等于上一行的列+上一行的列-1的值
                }
            }
            //输出
            for (int i = 0; i < rows; i++)
            {
                //打印空格
                for (int k = 0; k < rows - i; k++)
                {
                    Console.Write(" ");
                }
                for (int j = 0; j <= i; j++)
                {
                    Console.Write(nums[i][j] + " ");
                }
                Console.WriteLine();
            }


        }

    }
}




