﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //变量名称: my Achievement
            //常量：S
            //类：Class
            //接口：ICompare

            //求圆的面积

            int r = 2;

            double S = Math.PI * r * r;

            Console.WriteLine(S);

            //if else

            int b = 9;
            if (b >= 8 && b < 10)
            {
                Console.WriteLine("成绩优秀");
            }
            else if (b > 6 && b < 8)
            {
                Console.WriteLine("成绩良好");
            }
            else {
                Console.WriteLine("不及格你完了");
            }


            //  switch

            int a = 88;

            switch (a/10) {
                case 10:
                    Console.WriteLine("优秀");
                    break;
                case 9:
                    Console.WriteLine("优秀");
                    break;
                case 8:
                    Console.WriteLine("良好");
                    break;
                case 7:
                    Console.WriteLine("及格");
                    break;
                case 6:
                    Console.WriteLine("及格");
                    break;
                default:
                    Console.WriteLine("不及格");
                    break;

            }
        }

    }
}
