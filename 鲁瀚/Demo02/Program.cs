﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class Program
    {
        static void Main(string[] args)
        {
            //定义常量
            const double a = Math.PI;
            int b = 3;
            double result = a * b * b;
            Console.WriteLine("圆形的面积为 :{0}", result);
            Console.ReadKey();


            #region 变量命名规则
            /*
            1.变量命名规则遵循 Camel 命名法 例 student   studentName
            2.类的命名规则规则遵循 Pascal 命名法 例 Teacher
            3.接口命名规则遵循 Pascal 命名法 ICompare
            4.方法的命名遵循 Pascal 命名法 例AddUser
            */
            #endregion

            //if else 语句  用户登入
            string name, pwd;
            Console.WriteLine("请输入您的用户名");
            while (true)
            {
                name = Console.ReadLine();
                if (name.Equals("admin"))
                {
                    Console.WriteLine("请输入您的密码");
                    pwd = Console.ReadLine();
                    if (pwd == "123")
                    {
                        Console.WriteLine("登入成功");
                    }
                    else
                    {
                        while (true)
                        {
                            Console.WriteLine("密码输入错误 请重新输入");
                            if (pwd == "123")
                            {
                                Console.WriteLine("登入成功");
                            }
                            break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("请输入正确的用户名");
                }
            }


            //switch 语句  判断星期几 

            int week;

            while (true)
            {
                Console.WriteLine("请输入数字");
                int.TryParse(Console.ReadLine(), out week);
                if (week > 0)
                {
                    switch (week)
                    {
                        case 1:
                            Console.WriteLine("星期一");
                            break;
                        case 2:
                            Console.WriteLine("星期二");
                            break;
                        case 3:
                            Console.WriteLine("星期三");
                            break;
                        case 4:
                            Console.WriteLine("星期四");
                            break;
                        case 5:
                            Console.WriteLine("星期五");
                            break;
                        case 6:
                            Console.WriteLine("星期六");
                            break;
                        case 7:
                            Console.WriteLine("星期天");
                            break;
                        default:
                            Console.WriteLine("请输入有效的数字");
                            break;

                    }

                }
                else
                {
                    Console.WriteLine("请输入1-7数字");
                }


            }
        }
    }
}
